$(()=>{
    (function() {
    let rooms=[
        {name:"Predavaonica 1",
         itemscount:50},
         {name:"Predavaonica 2",
         itemscount:50},
         {name:"Predavaonica 3",
         itemscount:50},
         {name:"Predavaonica 4",
         itemscount:50},
         {name:"Predavaonica 5",
         itemscount:50},
         {name:"Predavaonica 6",
         itemscount:50},
         {name:"Portirnica",
         itemscount:50},
         {name:"Referada",
         itemscount:50}];
    
    for(let s of rooms){
        var novi= $('.sidebar2 ul').first().append(
            `<li>
            <a class="special1">${s.name}</a>
            <a class="special">Broj predmeta: ${s.itemscount}</a>
            </li>`
        );
    }
    })();  

    let poredak="silazno";
    $('.poredaj').css({cursor :"pointer"});
    $('.poredaj').on('click',()=>{
        if(poredak=="silazno"){
             $('.sidebar2 ul').html($('.sidebar2 ul li').sort(function(a,b){ return $(a).find(".special1").text()<$(b).find(".special1").text()}));
            $('.poredaj img').css({transform: "rotateX(180deg)"});
            poredak="uzlazno";
        }else{
            $('.sidebar2 ul').html($('.sidebar2 ul li').sort(function(a,b){ return $(a).find(".special1").text()>$(b).find(".special1").text()}));
            $('.poredaj img').css({transform: ""});
            poredak="silazno";
        }
    });

  
});