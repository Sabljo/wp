$(()=>{
    (function() {
        let menuItems=[
                {name:"Inventura",
                image:{
                    path:"./img/News.svg",
                    alt:"News"
                },
                link:"#inventura"},
                {name:"Inventar",
                image:{
                    path:"./img/Box.svg",
                    alt:"Box"
                },
                link:"#inventar"},
                {name:"Prostorije",
                image:{
                    path:"./img/Classroom.svg",
                    alt:"Classroom"
                },
                link:'#prostorije'},
                {name:'Zaposlenici',
                image:{
                    path:'./img/Contacts.svg',
                    alt:"Contacts"
                },
                link:"#zaposlenici" },
                {name:"Administracija",
                image:{
                    path:"./img/Services.svg",
                    alt:"Services"
                },
                link:"#administracija"}];
          
        let i=0;    
        for(let s of menuItems){
            $('.sidebar1 ul').first().append(
                `
                <li>
                <a href=${s.link}>
                <img src=${s.image.path} alt=${s.image.alt}>
                ${s.name}
                </a>
                </li>
                `
            );
        }
    })();
});