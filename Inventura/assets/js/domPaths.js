$(()=>{

function getPath (e) {
    var current = $(e);
    var path = new Array();
    while ($(current).prop("tagName") != "BODY") {
        path.push(current);
        current = $(current).parent();
    }
    path.reverse();
    return path;
}
var x = 0;
var target1;
var target2;
$('body').click(function(e){
    x++;
    if(x == 1){
        target1=e.target;
    }
    if (x == 2) {
        target2=e.target;
        //pronađemo puteve za svaki od body-a do targeta
        var path1 = getPath(target1);
        var path2 = getPath(target2);
        //izbacujemo sve elemente s početka koji su jednaki u oba puta, ali tako da ostavimo
        //samo jedan koji jednak u oba puta
        for(let i=1;i<Math.min(path1.length,path2.length);){
            if(path1[i].get(0)==path2[i].get(0) && path1[i-1].get(0)==path2[i-1].get(0)){
                path1.shift();
                path2.shift();
            }else{
                i=i+1;
            }
        }
        //prvi put reversamo
        path1.reverse();
        var konacni_put="";
        var i=0;
        for(let s of path1){
            if(i==path1.length-1){
                konacni_put+=($(s).prop("tagName").toLowerCase());
            }else{
            konacni_put+=($(s).prop("tagName").toLowerCase())+" -> ";
            }
            i=i+1;
        }
        //izbacimo zadnji iz puta drugog targeta jer je on zajednicki u oba puta
        path2.shift();
        for(let s of path2){
            konacni_put+=" -> "
            konacni_put+=($(s).prop("tagName").toLowerCase());
        }
        if(konacni_put.length==1){
            konacni_put=" ";
        }
        x=0;
        console.log(konacni_put);

    }
});
   
});
